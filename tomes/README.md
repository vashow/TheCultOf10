# The Tomes
This is the main entry point into the Tomes of 10, containing links to sermons and/or games/quizzes to test your faith of the fisty.  


## Composition (Will be updated at the Hand's earliest convenience):

+ [Preface](/vashow/TheCultOf10/src/branch/main/tomes/preface.md).  
+ [1st Sermon: The Building Blocks.](/vashow/TheCultOf10/src/branch/main/tomes/1st_sermon.md).  
