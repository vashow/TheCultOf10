# The Second Sermon: The Commandments.

BigHand is not such an understanding deity unfortunately. In this sermon am going to lay out rules / laws / unegotiables in BigHand's cruel system.  

But before all that we are first going to witness the philosophy of BigHand first hand :laugh:.  

### 2.1 The Balance of Fist.

BigHand gave all of man the main hand, that we have already seen from the first sermon. What about the other hand?  
BigHand knowing that man's life is full of ups and downs saw it fit to provide us with a method of balance. The helper hand, the other hand. Some insult it by saying it is the less-dominant one, but no. It is the *helper* as BigHand has decreed. How does it help though?  

#### 2.1.1 Illustration

Say you finish all the mangoes but still being hungry see your friend has 34 (three fisties and four) mangoes all to themselves. Being your friend though he is willing to **lend** you some, he knows by the time yours grow his might be already spent and you will be in abundance then.  
Hhmm... That seems like a good plan, a win-win. But how do we record this transaction?  
*Exactly* BigHand gave us a *helper* hand to record this.
So your friend gives you 10 mangoes (a fisty), thus you owe your friend a fisty of mangoes to be returned at a future date.  
BigHand then gave us a helper symbol (`-`, negative) to represent this phenomena. In BigHand's speak we say you have `-10` mangoes currently, that is a negative fisty of mangoes.  

### Continuum ...
These negative numbers (based off the *helper*'s hand notation) grow to the opposite side of the positive numbers(based off the *main*'s hand notation). Imagine a line that represent our BigHand notation with its length growing to the right proportionately to BigHand's number:  
```
_________________________
0  1  2  3  4  10  11 ...
```
These follow the positive (main hand) notation and grow to the right.  

To represent the other side we project a mirror-image about `none` and use the helper's hand side and grow to the left like so:  

```
_____________________________________
-10 -4 -3 -2 -1 0  1  2  3  4  10 ...
```

The combination of these numbers (both the main, helpers and the none value) form a type called?
```
integers
```
You should notice the system grows out of none `0` thus the symbol is also named *the gate to balance*.  

## 2.2 The First Commandment: Comparison.

BigHand set out in their holy teachings that the order in which numbers grow out of `0` is uniform and predictable. That is `10` (a fisty) shall always come after `4` when both are arranged in an ascending order and the inverse is also true.  
To ease the communication of this, BigHand provided us with this symbol, `>` (looks like it's narrowing down to the lesser number) to represent *greater than*.  

What does this `24 (two fisties and four) > 13 (fisty and three)` say in man language?  
```
Two fisties and four is greater than fisty and three.
```
To communicate the inverse BigHand gave us the opposing symbol `<` (looks like it's widening up to the greater number) to represent *less than*.  

What does this `-23 < 200` say in man language?  
```
Negative two fisties and three is less than two fifists.  
```

These two symbols, `>` and `<` are known as the **comparison** *operators*.  

**Operators**
!NewVocabAlert!  
- Operator - this is a symbol that represents a known behaviour that has been fully proved. Don't worry too much about this now just know that in the previous section in this statement `-23 < 200` the *less than* symbol is the operator in the statement and that for the stated to be true the number to the left of the `<` operator **MUST** be *less than* the number to the right of the operator.  

## 2.3 The Second Commandment: Equality.

On recognizing the presence of variance/difference, BigHand yet again provided us with another tool so to decode their beautiful language. The `=` equality operator which equates the right side of it to the left side.  
That is, this statement `10 = 10` is and will always mean that?
```
A fisty will always be equal to a fisty
```
This is a brief and true commandment that you must follow.  

***  
!SideBar!  
Welcome to the sidebar, where I get to give a small injections of BigHand propaganda so as to push the BigHand Agenda.  
Be on the lookout for them and please read the propaganda :palms_up_together:. Their HighHandedness says if I don't radicalize more members they will deduct my even less than worthless 100_00000 tokens. I wonder why I even care at all :roll_eyes: since they are useless in the real world anyways. But since am doing the work I might as well get them.  
***  

## 2.4 The Third Commandment: Addition.

The next symbol we are going to introduce from the holy texts is the `+` symbol which signifies addition, the act of combining two magnitudes.  

Now that we have a working system to represent any magnitude as well as also communicate their hierarchical relations to each other, we can begin consuming BigHand's less basic teachings.  
We start with the `addition` commandment, if you have 4 bananas and I add you four more how many bananas will you have?  

#### Listing 2.4.1
You have |||| bananas I give you |||| more bananas that is combine the two together and you get?  
(Let's count them together)
|||| + |||| = ~~||||~~ ||| => 13 that is one fisty and three (or fisty-three in short).  

The `+` operator is used to describe this behaviour of combining.  
Representing the operation we illustrated in man symbols is then?  
```
4 + 4 = 13
```
### 2.4.1 Number line. 
One of the tools that help us understand BigHand's system is the number line. We can use this line to represent a number by making the length equal to the magnitude of the number. Example:

```
4 in number-line is?
 --------->
 | | | | |  
 0   2   4 
```

The arrow shows its direction so -4 is represented as?

```
 <---------
  | | | | |  
 -4       0
```

Using this tool we can observe the effects of addition even more clearly, see for yourself:

```
4 + 3 = 12


 --------->            ------->
 | | | | |      +      | | | |
 0   2   4             0     3
         --------------->
       = | | | | | | | |
         0       4     12
                 |     |
                 |    Answer.
                 |
                 -> Note that the number in the left hand side (4) of the
                    "+" sign is pushed forward exactly 3 spaces infront
                    which is the same magnitude of the right hand side. ->.

```
So from the experiment we can conclusively say that; the effect of addition in the number line is pushing the left hand side away from the balance (0) towards the right by the same magnitude of the right hand side number.  

!CoolFactoiD!

The addition process produces the same result even when the order around the `+` sign are interchanged that is:  
`2 + 3 = 3 + 2`     (You can try it using your hands to confirm this :smiley:)  
One of the HighHands told me this is known as *Commutativity*. This behaviour is special to addition.

!CoolFactiodOver!

Now that we have got the gist of addition let us create basic tables that will form the basis of the whole system. When BigHand selects the chosen those missing this on their bodies shall not see the promised.

```
   1.
1 + 1  = 2
1 + 2  = 3
1 + 3  = 4
1 + 4  = 10
1 + 10 = 11

   2.
2 + 1  = 3
2 + 2  = 4
2 + 3  = 10
2 + 4  = 11
2 + 10 = 12
   3.
3 + 1  = 4
3 + 2  = 10
3 + 3  = 11
3 + 4  = 12
3 + 10 = 13
   4.
4 + 1  = 10
4 + 2  = 11
4 + 3  = 12
4 + 4  = 13
4 + 10 = 14
   10.
10 + 1  = 11
10 + 2  = 12
10 + 3  = 13
10 + 4  = 14
10 + 10 = 20
  ...

```

## 2.10 Commandment 4: Subtraction.  
The opposite of the addition is subtraction, the act of *taking away*. To make it easier to grasp remember the helper hand? You borrowed a fisty of mangoes from your friend earlier but since your mangoes have grown and you have an abundance we want to return the fisty of mangoes.  
We can represent this with.
```
-10 initial balance of mangoes (you owe, so your balance is negative already).
10 the number of harvested mangoes to be returned to your friend.

The operation is then expressed as:

-10 + 10 = 0 (none) basically you have balanced your numbers (0 - the gate of balance).
```
The commandment thus reads: *Adding a positive (main hand) number to a negative(helper) number pulls it towards the balance (0).*  
As BigHand allows man to interpret best their commandments many have resorted to represent this as:
```
10 - 10 = 0
```
Which translates to *subtracting a fisty from a fisty equals to none*.

We can now create a similar table to the addition table to extrapolate this behaviour.  

**Subtraction table**  
```
   1.
1 - 1   = 0
1 - 2   = -1
1 - 3   = -2
1 - 4   = -3
1 - 10  = -4
   2.
2 - 1   = 1
2 - 2   = 0
2 - 3   = -1
2 - 4   = -2
2 - 10  = -3
   3.
3 - 1   = 2
3 - 2   = 1
3 - 3   = 0
3 - 4   = -1
3 - 10  = -2
   4.
4 - 1   = 3
4 - 2   = 2
4 - 3   = 1
4 - 4   = 0
4 - 10  = -1
   10.
10 - 1  = 4
10 - 2  = 3
10 - 3  = 2
10 - 4  = 1
10 - 10 = 0
``` 

## 2.11 Getting in the trenches.

We have to agree that the tables above are true please :praying:. If you doubt any just slowly use the instruments BigHand gave us (the main and helper hands) and confirm the calculations above.  
***
!PleaseNote! 
ALL the sums above CAN be simulated and confirmed by using the main and helper hands ALONE. If you cannot please pray harder and redo the first verses again :tear:.
***

Once we agree on the tables above we can now combine what we learnt in the first sermon and build on the commandments.  

### 2.12 Big Numbers.

#### 2.12.1 Addition.
Now that BigHand has guided us this far we need to be comfortable handling bigger numbers. Don't worry though am here to guide you through the murk. 

!PoP QuiZ!
What is 3 + 4 ?

```
12
```

So far all our operations we have been able to use our hands/fists to simulate them but to grow we must deal with bigger numbers. BigHand showed us in the first verses the power of the `wrap` let us squeeze it out further.  
First `3 + 4 = 12` using the finger system, ||| + |||| = ~~||||~~ || that is 3 provides a stick to wrap and is left with 2 fingers making a fisty and two (or `fisty two` in short).  
Once we get the basic additions into memory we can now tackle larger numbers. What are *larger/bigger* numbers?

```
    Normal.
10      => fisty.
100     => fifisty.
1000    => gisty.
10000   => gigisty.

Large.
1_00000     => histy (entry to large numbers: has a fisty of nones).
10_00000    => hihisty
100_00000   => jisty
1000_00000  => jijisty

...
```

You get the gist (and for you like me who didn't, the naming grows in alphabetical order with the repeater having exactly one more nones in its body).  
Lets not deal with jisties for now or even `misties` :skull:. The most powerful aspect of BigHand's language is that as long as his rules reign supreme small or large numbers all bow to BigHand's might. So as long as you know how to add together the main hands numbers you can add any number in the system.  
*Example:*
```
3 + 4 = 12
33 + 44 = ?
```
Let me show you a cool technique to do this, first arrange the numbers one above the other, preferrably the greater above the lesser (aesthetics sake). Like this: 

```
 44
+33
```
Now the problem is easier all you have to do is add vertically where you only need to use the main hand numbers which are easy to compute (Go back and cram those addition/subtraction tables please). But one last thing before we continue, remember the wrap once a  number aproaches the holy fisty it wraps(overflows to the left) instead.
```
  (1)            => wrap/overflow
   4 4
+  3 3
  -----
   132
     |
     It overflows here and is added to the number to its left.
  -----
```
Easy to follow right? :woozy_face:  
Okay then some few more examples then.  
```
(1)
   3
 + 2
 ----
 1 0
   |
  It overflowed here.


 (1)
  1  3
  +  2
------
  2  0
     |
    Yeap you guessed it.


(1)
   4 2
 + 2 1
 ------
 1 1  3
   |
   This time it overflowed here.


 (1) (1)
      3  4
   +  2  4
   -------
   1 1  3
   -------


(1)   (1)
    4  2  3
  + 2  1  4
-------------
 1  1  4  2
-------------


```

***
TIP
---
If you have trouble doing the additions please write out the addition tables and redo the sums with the aid of the tables. We are targetting repetition, it'll come.
***

#### 2.12.2 Subtraction.

!Digression!  
Before we tackle subtraction let us digress and realise that in subraction the order of the values around the sign matters that is:  
`2 - 3` ~~=~~ `3 - 2`  
Can you prove why?
```
2 - 3 = -1
while
3 - 2 = 1
therefore
```
`2 - 3` ~~=~~ `3 - 2`  
So be careful with the orders in regards to this operator.  
!DigressionOver!  


The subtraction sign takes the magnitude of the right side from the left's. In a numberline this phenomena can be observed:
```
3 - 10 = ?
       = -2


 -------------              ___________ 
 | | | | | | |      -       | | | | | |   
-3     0     3             10         20
           -----------
       =   | | | | | |
          -2   0     3
           |         |
           Answer    |
                  <- Three is pulled to the left <-.

```

So in a number line subtraction always pulls the number to the left **but only if** the number is positive if the number is negative it does the opposite that is:



```
3  -  -10 = ?
          = 13

 -------------              ___________ 
 | | | | | | |      -       | | | | | |   
-3     0     3            -10         0
           -----------
       =   | | | | | |
           3         13
           |         |
           |         Answer.
           |
   -> Three is pushed to the right just like in addition ->.
```
Since this last operation behaves the same as if you added the absolute (is represented by surrounding the symbol with `|` so the absolute of -1 is written as |-1| and is understood as? `|-1| = 1`, that is it ignores negativity) of the negative number, to the positive one. Thus any time you see `1  -  -23` it is the same as ` 1 + 23 `. 






































!Disclaimer!  
From here on we are going to stop hand-holding you and let you take charge of your faith, but do not tire BigHand will guide you. All they ask is for you to spot the patterns.  

## 2.11 Building off the main commandments.

### Multiplication.
Remember when we were counting mangoes? In the first sermon.  
We said this ~~||||~~ ~~||||~~ | was `two fisties and one` it's so easy to spot the fisties, how many are they?

```
2
```
Since it is so easy to spot them, i.e.
```
~~||||~~ => fisty, 10.
~~||||~~ ~~||||~~ => Two fisties, 20.
~~||||~~ ~~||||~~ ~~||||~~ => Three fisties, 30.
~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~ => Four fisties, 40.
~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~ => fifisty, 100.

```






























