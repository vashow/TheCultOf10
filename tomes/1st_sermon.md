## 1st Sermon: **"Beginning the offence of 10"**.

### Arithmetic.

Hi10. We are under trouble and we need your help to revive the cult of 10. Men overeached. We should have never gone beyond what BigHand gave us. 

#### Number System.

From the moment we are born BigHand gave us the perfect instrument. Raise your hand right now. Good. That's our sign to offer respect and deference to BigHand. Now we can begin your education.  

##### BigHand's system. (10)

When you want to count how many mangoes there are in a tree, BigHand provides a simple and memorable system to enumerate and memorize this data easily. 

###### BigHand's verse 1;

How to count mangoes:

|  
||  
|||  
||||  
~~||||~~  
...

Each stick `|` represents a finger, each curling to form a fist which eventually forms up to a fist through the crossing of the ~~thumb~~.
BigHand gave us one *main* hand and we should respect that ~~||||~~.  

Okay let's say you have more mangoes what then? Well BigHand gave us means to store and represent this so get out your paper and let's start counting mangoes:  

~~||||~~ |  
~~||||~~ ||  
~~||||~~ |||  
~~||||~~ ||||  
~~||||~~ ~~||||~~  
~~||||~~ ~~||||~~ |  
~~||||~~ ~~||||~~ ||  
~~||||~~ ~~||||~~ |||  
~~||||~~ ~~||||~~ ||||  
~~||||~~ ~~||||~~ ~~||||~~  
...  

By using BigHand's system we can continue counting ad infinitum (I think this means forever).  
Okay you should notice a pattern forming. The sticks are forming a nice satisfying pattern? Uhm yes... Sort of. Let us define this system further and make it even more elegant.  
As the system is composed of a tiny set we can assign each quantity a symbol and name.

###### BigHand's Verse 2;

How to label the main hand:

|       - 1, one.  
||      - 2, two.  
|||     - 3, three.  
||||    - 4, four.  

Wait before I give you the final symbol can you spot it?  
I'd be surprised if you did cause it's actually nothing (see what I did there).  


_       - 0, none.  
|       - 1, one.  
||      - 2, two.  
|||     - 3, three.  
||||    - 4, four.  

If I eat all the mangoes what will remain?  

The set `{0, 1, 2, 3, 4}` is crux of the second verse. With these symbols we can represent each of thy holy fingers. Due to their brevity these symbols are also known as the *base numbers*.    
Notice that even as the fists of fingers grow we can easily contextualize them as our minds can easily decode that ~~||||~~ represents a fist.    

~~||||~~ ~~||||~~ |||   - This represents 2 fists and 3 fingers. 

!PoP QuiZ!
What does these combination represent?  

~~||||~~ ~~||||~~ ~~||||~~ || 

`3 fists and 2 fingers`. 

*The Big Reveal*    
By now I know you are already seeing the problem I have been skirting around; representing BigHand's holiest number.  
BigHand, in one of his generocities gave us a hidden clue on how we can use the base numbers to represent the holy quantity "~~||||~~" perfectly.  
Notice that when the base numbers approach the holy number, BigHand forces them to wrap around through the *sacred* ~~strike through stick~~ (thumb).  
BigHand gave us the clue have you seen it?  
Turns out to represent a holy fist perfectly we copy the holy designs and **wrap** it around.  
So we continue counting with:  

_                           - 0, none.  
|                           - 1, one.  
||                          - 2, two.  
|||                         - 3, three.  
||||                        - 4, four.  
~~||||~~                    - 10, fist. // Notice the wrap :eyes:.  
~~||||~~ |                  - 11, fisty and one.  
~~||||~~ ||                 - 12, fisty and two.  
~~||||~~ |||                - 13, fisty and three.  
~~||||~~ ||||               - 14, fisty and four.  
~~||||~~ ~~||||~~           - 20, two fisties. // Second wrap.  
~~||||~~ ~~||||~~ |         - 21, two fisties and one.  
~~||||~~ ~~||||~~ ||        - 22, two fisties and two.  
~~||||~~ ~~||||~~ |||       - 23, two fisties and three.  
~~||||~~ ~~||||~~ ||||      - 24, two fisties and four.  
~~||||~~ ~~||||~~ ~~||||~~  - 30, three fisties. // Third  
...  
~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~ ||||        => 44, four fisties and four.  
~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~ ~~||||~~    => 100, fifist.  

From the pattern above it is clear that as every number approaches the holy fist it wraps around into the greater side to its left.  
So the next number after 134 is?

```
140
```
Turns out man is better at using symbols than the preferred BigHand's finger system. So, from here on to the end we are going to use utilise the base numbers which are?  

```
0, 1, 2, 3, 4
```

###### BigHand's verse 3: Significance;

Notice something with the holy system. The ease at which we can recognise the number of fisties.  
Let me elaborate: 
From this "~~||||~~ ~~||||~~ ||||" it is easy to see there are at least two fisties easily distinguished since it is always wrapped by the ~~thumb~~ while the fingers normally require manual counting to accurately know their quantity, BigHand designed the number of fisties to always be easy to spot.  
This behaviour subtly relies on a certain beauty to BigHand's system. To start to understand BigHand's magnificense we must understand and obey the third verse which decrees as follows:  
***
Greatness increases the further you are from the base number
***
To understand we must continue to dissect the finger system further. You might have figured it out (or not) but there are two ways of representing BigHand's system. So far we have been using the lefty method.  
Let me explain:  
This is ~~||||~~ |||?  
`one fist and three` or in short `fisty three`  
Which we have been representing as?  
`13`  
We could also have represented the same thing with the righty method (it populates to the right instead :nauseated_face:) as:  
||| ~~||||~~  
And written as  
`3 fingers and a fisty`  
Which will be further represented as what in the *righty* way?  
`31`  

Not even the higher rank members of the `cult of fisty` agree on the set method. Though for consistency and prevention of confusion we are going to use the l-faction which follows lefty method: *the further you are to the left of the relative finger the greater you become and the inverse is also true*.  
So in this record, `134`, which number has the highest significance according to the lefty method?  
`1`    - which in our case represents  a fifist (100 pronounced as fy.fi.st) while the rest follow respectively representing?  
`3` - three fisties.  
`4` - four fingers.  

With these three verses we have formed a basis for our system. Now we get to explore further patterns that appear within BigHand's system.  
Keep faith and follow the Fisty.  
