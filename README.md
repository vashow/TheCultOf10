# TheCultOf10

This project is composed of two main features:
1. The Tomes of 10 - which contain the sermons/excerpts from BigHand's teachings.
2. tester - which is the Rust program that is supposed to provide tests at the end of every sermon to gauge your knowledge on the material covered (Will begin to be used after the second sermon).

## Paths.
This project is for the curious and bored. To begin start [here](/vashow/TheCultOf10/src/branch/main/tomes/README.md) and follow along the links towards your inculcation into TheCultOf10.
